package it.dan.library.stringify;

import it.dan.library.stringify.callback.FormatCallback;
import it.dan.library.stringify.callback.SubstrFormatCallback;
import it.dan.library.stringify.parse.exception.MalformedInputException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * <!-- Created by d.carnovale on 04/12/2014. -->
 */
public class StringifyTest {

    private Stringify stringify;

    private static final String SIMPLE_MASK_STRING = "this is a #name string";

    @Before
    public void setUp() {

        FormatCallback myConstCallback = new FormatCallback() {
            @Override
            public String parse(String... args) {
                if (args != null && args.length > 0) {
                    throw new IllegalArgumentException("const method accept 0 argument");
                }

                return "my const";
            }
        };

        stringify = new Stringify(SIMPLE_MASK_STRING);
        stringify.addFunction(SubstrFormatCallback.SUBSTR_NAME, new SubstrFormatCallback());
        stringify.addFunction("const", myConstCallback);

    }

    /**
     * Test a simple string using mask syntax.
     *
     * @throws MalformedInputException
     */
    @Test
    @Ignore
    public void testParseMask() throws MalformedInputException {

        stringify.addReplacer("name", "fabulous");

        // apply a mask
        Assert.assertEquals("this is a fabulous string", stringify.parse());

    }

    /**
     * Test a simple string without using any formatter syntax.
     *
     * @throws MalformedInputException
     */
    @Test
    public void testParseSimple() throws MalformedInputException {

        // simple: nothing to do
        Assert.assertEquals(SIMPLE_MASK_STRING, stringify.parse());

        // simple: nothing to do
        Assert.assertEquals("this is a simple string", stringify.parse("this is a simple string"));

    }

    /**
     * Test a string using one function in formatter syntax.
     *
     * @throws MalformedInputException
     */
    @Test
    public void testParseOneFunction() throws MalformedInputException {

        // last char is not a function end
        Assert.assertEquals("this is a my const", stringify.parse("this is a @const()"));

        // last char is a function end
        Assert.assertEquals("this is a my const!", stringify.parse("this is a @const()!"));

    }

    /**
     * Test a string using more functions in formatter syntax.
     *
     * @throws MalformedInputException
     */
    @Test
    public void testParseMoreFunctions() throws MalformedInputException {

        // last char is not a function end
        Assert.assertEquals("this is a part of test or const my const", stringify.parse("this is a part of @substr(mytest, 2, 4) or const @const()"));

        // last char is a function end
        Assert.assertEquals("this is a part of test or const my const!", stringify.parse("this is a part of @substr(mytest, 2, 4) or const @const()!"));

    }

    /**
     * Test a string using escape char formatter syntax.
     *
     * @throws MalformedInputException
     */
    @Test
    public void testParseSimpleEscape() throws MalformedInputException {

        // last char is not a function end
        Assert.assertEquals("this is an escaped char: @!", stringify.parse("this is an escaped char: \\@!"));

        // last char is a function end
        Assert.assertEquals("this is an escaped char: @", stringify.parse("this is an escaped char: \\@"));

    }

    /**
     * Test a string using escape char and function in formatter syntax.
     *
     * @throws MalformedInputException
     */
    @Test
    public void testParseFunctionEscape() throws MalformedInputException {

        // last char is not a function end
        Assert.assertEquals("this is a function with escaped char inside lo,wo!", stringify.parse("this is a function with escaped char inside @substr(hello\\,world, 3, 5)!"));

        // last char is a function end
        Assert.assertEquals("this is a function with escaped char inside lo,wo", stringify.parse("this is a function with escaped char inside @substr(hello\\,world, 3, 5)"));

    }

    @Test(expected = MalformedInputException.class)
    public void testFunctionInsideFunction() throws MalformedInputException {

        // actually function in function is not accepted!
        Assert.assertEquals("this is a function const that success as unexpected!", stringify.parse("this is a function @substr(@const(\\), 1, 5) that success as unexpected!"));

    }

    /**
     * Test a simple string with escape char at end of the string.
     *
     * @throws MalformedInputException
     */
    @Test(expected = MalformedInputException.class)
    public void testParseSimpleEscapeFail() throws MalformedInputException {

        // missing char after escape \\ char
        stringify.parse("this is simple failing string \\");

    }

    /**
     * Test a simple string with function char at end of the string.
     *
     * @throws MalformedInputException
     */
    @Test(expected = MalformedInputException.class)
    public void testParseFunctionFail() throws MalformedInputException {

        // missing name after @ char
        stringify.parse("this is simple failing string with incomplete function @");

    }

    /**
     * Test a simple string with function name at end of the string.
     *
     * @throws MalformedInputException
     */
    @Test(expected = MalformedInputException.class)
    public void testParseFunctionNameFail() throws MalformedInputException {

        // missing () at end of function
        stringify.parse("this is simple failing string with incomplete function @const");

    }

    /**
     * Test a simple string with an invalid function name.
     *
     * @throws MalformedInputException
     */
    @Test(expected = MalformedInputException.class)
    public void testParseFunctionNameInvalid() throws MalformedInputException {

        // function char @ is not allowed in function name
        stringify.parse("this is simple failing string with incomplete function @co@nst()");

    }

    /**
     * Test a simple string with an invalid function name.
     *
     * @throws MalformedInputException
     */
    @Test(expected = MalformedInputException.class)
    public void testParseFunctionNameInvalid2() throws MalformedInputException {

        // escape char \ is not allowed in function name
        stringify.parse("this is simple failing string with incomplete function @co\\nst()");

    }

    /**
     * Test a simple function that return an exception.
     *
     * @throws MalformedInputException
     */
    @Test(expected = IllegalArgumentException.class)
    public void testParseFunctionArgumentsFail() throws MalformedInputException {

        // const want 0 arguments
        stringify.parse("this is a function @const(unexpected) that fail");

    }
}
