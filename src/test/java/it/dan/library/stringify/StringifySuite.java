package it.dan.library.stringify;

import org.junit.runners.Suite;

/**
 * <!-- Created by d.carnovale on 04/12/2014. -->
 */
@Suite.SuiteClasses({StringifyTest.class, SubstrFormatCallbackTest.class, StrlenFormatCallbackTest.class})
public class StringifySuite {
}
