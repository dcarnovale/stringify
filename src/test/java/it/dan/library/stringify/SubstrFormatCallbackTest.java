package it.dan.library.stringify;

import it.dan.library.stringify.callback.SubstrFormatCallback;
import it.dan.library.stringify.parse.exception.MalformedInputException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * <!-- Created by d.carnovale on 04/12/2014. -->
 */
public class SubstrFormatCallbackTest {

    private Stringify stringify;

    @Before
    public void setUp() {
        stringify = new Stringify();
        stringify.addFunction(SubstrFormatCallback.SUBSTR_NAME, new SubstrFormatCallback());
    }

    /**
     *
     * @throws MalformedInputException
     */
    @Test
    public void testSimpleParse() throws MalformedInputException {

        Assert.assertEquals("hello", stringify.parse("@substr(hello,0,5)"));
        Assert.assertEquals("ll", stringify.parse("@substr(hello,2,2)"));

        Assert.assertEquals("el", stringify.parse("@substr( hello,2,2)"));

        // future
        // Assert.assertEquals("ll", stringify.parse("@substr( hello,2,2)"));
        // Assert.assertEquals("ll", stringify.parse("@substr( 'hello', 2, 2 )"));

    }

    /**
     *
     * @throws MalformedInputException
     */
    @Test
    public void testEscapeParse() throws MalformedInputException {

        Assert.assertEquals("o,t", stringify.parse("@substr(hello\\,test,4,3)"));

    }

    /**
     *
     * @throws MalformedInputException
     */
    @Test(expected = IllegalArgumentException.class)
    public void testMoreArgumentsParse() throws MalformedInputException {

        stringify.parse("@substr(hello,test,2,2)");

    }

    /**
     *
     * @throws MalformedInputException
     */
    @Test(expected = IllegalArgumentException.class)
    public void testWrongArgumentsTypeParse() throws MalformedInputException {

        stringify.parse("@substr(hello,test,2)");

    }
}
