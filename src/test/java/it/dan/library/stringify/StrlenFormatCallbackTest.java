package it.dan.library.stringify;

import it.dan.library.stringify.callback.StrlenFormatCallback;
import it.dan.library.stringify.parse.exception.MalformedInputException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * <!-- Created by d.carnovale on 04/12/2014. -->
 */
public class StrlenFormatCallbackTest {

    private Stringify stringify;

    @Before
    public void setUp() {
        stringify = new Stringify();
        stringify.addFunction(StrlenFormatCallback.SUBSTR_NAME, new StrlenFormatCallback());
    }

    /**
     *
     * @throws MalformedInputException
     */
    @Test
    public void testSimpleParse() throws MalformedInputException {

        Assert.assertEquals("5", stringify.parse("@strlen(hello)"));
        Assert.assertEquals("11", stringify.parse("@strlen(hello world)"));
        Assert.assertEquals("13", stringify.parse("@strlen( hello world )"));

        // future
        // Assert.assertEquals("11", stringify.parse("@strlen( hello world )"));
        // Assert.assertEquals("13", stringify.parse("@strlen(' hello world ')"));

    }

    /**
     *
     * @throws MalformedInputException
     */
    @Test
    public void testEscapeParse() throws MalformedInputException {

        Assert.assertEquals("11", stringify.parse("@strlen(hello\\,world)"));

    }

    /**
     *
     * @throws MalformedInputException
     */
    @Test(expected = IllegalArgumentException.class)
    public void testMoreArgumentsParse() throws MalformedInputException {

        stringify.parse("@strlen(hello,test)");

    }

    /**
     *
     * @throws MalformedInputException
     */
    @Test(expected = IllegalArgumentException.class)
    public void testLessArgumentsParse() throws MalformedInputException {

        stringify.parse("@strlen()");

    }
}
