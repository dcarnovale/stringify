package it.dan.library.stringify.parse.exception;

/**
 * <!-- Created by dcarnovale on 06/12/2014. -->
 *
 * @author d.carnovale
 * @version 1.0
 * @since 1.0
 */
public class MalformedInputException extends Exception {

    /**
     *
     * @version 1.0
     * @since 1.0
     *
     */
    public MalformedInputException() {
        super();
    }

    /**
     *
     * @param errorMessage
     *
     * @version 1.0
     * @since 1.0
     */
    public MalformedInputException(final String errorMessage) {
        super(errorMessage);
    }

    /**
     *
     * @param throwable
     *
     * @version 1.0
     * @since 1.0
     */
    public MalformedInputException(final Throwable throwable) {
        super(throwable);
    }

    /**
     *
     * @param errorMessage
     * @param throwable
     *
     * @version 1.0
     * @since 1.0
     */
    public MalformedInputException(final String errorMessage, final Throwable throwable) {
        super(errorMessage, throwable);
    }
}