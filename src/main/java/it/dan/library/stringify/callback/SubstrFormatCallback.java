package it.dan.library.stringify.callback;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <!-- Created by d.carnovale on 07/12/2014. -->
 *
 * @author d.carnovale
 * @version 1.0
 * @since 1.0
 */
public class SubstrFormatCallback implements FormatCallback {

    /**
     *
     *
     * @version 1.0
     * @since 1.0
     */
    public static final String SUBSTR_NAME = "substr";

    /**
     *
     */
    private static final Logger logger = Logger.getLogger(SubstrFormatCallback.class.getCanonicalName());

    /**
     *
     * @param args
     * @return
     *
     * @version 1.0
     * @since 1.0
     */
    @Override
    public String parse(final String... args) {
        if (args == null || args.length != 3) {
            final String errorMessage = SUBSTR_NAME + " method accept 3 arguments";
            logger.severe(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        final int offset = Integer.parseInt(args[1].trim());
        final int len = Integer.parseInt(args[2].trim());
        final String substring = args[0].substring(offset, offset + len);

        if (logger.isLoggable(Level.FINER)) {
            logger.finer("args[0]: " + args[0]);
            logger.finer("offset: " + offset);
            logger.finer("len: " + len);
            logger.finer("substring: " + substring);
        }

        return substring;
    }
}
