package it.dan.library.stringify.callback;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * <!-- Created by d.carnovale on 07/12/2014. -->
 *
 * @author d.carnovale
 * @version 1.0
 * @since 1.0
 */
public class StrlenFormatCallback implements FormatCallback {

    /**
     *
     *
     * @version 1.0
     * @since 1.0
     */
    public static final String SUBSTR_NAME = "strlen";

    /**
     *
     */
    private static final Logger logger = Logger.getLogger(StrlenFormatCallback.class.getCanonicalName());

    /**
     *
     * @param args
     * @return
     *
     * @version 1.0
     * @since 1.0
     */
    @Override
    public String parse(final String... args) {
        if (args == null || args.length != 1) {
            final String errorMessage = SUBSTR_NAME + " method accept 1 arguments";
            logger.severe(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        final String strlength = String.valueOf(args[0].length());

        if (logger.isLoggable(Level.FINEST)) {
            logger.finest("arg[0]: " + args[0]);
            logger.finest("strlength: " + strlength);
        }

        return strlength;
    }
}
