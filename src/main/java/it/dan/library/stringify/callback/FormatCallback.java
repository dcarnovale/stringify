package it.dan.library.stringify.callback;

/**
 *
 * <!-- Created by d.carnovale on 03/12/2014. -->
 *
 * @author d.carnovale
 * @version 1.0
 * @since 1.0
 */
public interface FormatCallback {

    /**
     *
     * @param args
     * @return
     *
     * @version 1.0
     * @since 1.0
     */
    String parse(String... args);
}
