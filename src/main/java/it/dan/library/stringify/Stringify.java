package it.dan.library.stringify;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import it.dan.library.stringify.callback.FormatCallback;
import it.dan.library.stringify.parse.exception.MalformedInputException;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Format a string using a special syntax to define more flexible formatting rule.<br>
 * The format must respects the following rule:
 * <ul>
 *   <li>a function start with @ char and must follow a name (1 char at least). it must terminate with ([eventually arguments comma separated])</li>
 *   <li>special chars must be escaped by \ char</li>
 *   <li>function inside function is not allowed</li>
 * </ul>
 *
 * <!-- Created by d.carnovale on 03/12/2014. -->
 *
 * @author d.carnovale
 * @version 1.0
 * @since 1.0
 */
public class Stringify {

    private static final Logger logger = Logger.getLogger(Stringify.class.getCanonicalName());
    private String maskedString;
    private Map<String, FormatCallback> functions;
    private Map<String, String> replacers;

    /**
     * Create an new Stringify object.
     *
     * @since 1.0
     */
    public Stringify() {
        functions = new HashMap<String, FormatCallback>();
        replacers = new HashMap<String, String>();
    }

    /**
     * Create an new Stringify object with a mask string. The mask syntax is specified in the class documentation.
     *
     * @param maskedString
     *
     * @since 1.0
     */
    public Stringify(final String maskedString) {
        this();
        this.maskedString = maskedString;
    }

    /**
     *
     * @return
     * @throws MalformedInputException
     *
     * @since 1.0
     */
    public String parse() throws MalformedInputException {
        logger.fine("parse()");
        final String parseString = mask(this.maskedString);
        if (logger.isLoggable(Level.FINER)) {
            logger.finer("parseString: " + parseString);
        }
        return parse(parseString);
    }

    /**
     *
     * @param parseString
     * @return
     * @throws MalformedInputException
     *
     * @since 1.0
     */
    public String parse(final String parseString) throws MalformedInputException {
        if (logger.isLoggable(Level.FINE)) {
            logger.fine("parse('" + parseString + "')");
        }
        Preconditions.checkNotNull(parseString, "maskedString cannot be null");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(parseString), "maskedString cannot be empty");

        StringBuilder parsedString = new StringBuilder();
        StringBuilder functionName = new StringBuilder();
        StringBuilder argumentValue = new StringBuilder();
        List<String> functionArguments = new ArrayList<String>();

        char[] maskedCharacters = parseString.toCharArray();
        Action action = Action.DOCUMENT_BEGIN;
        int i = 0;
        while (action != Action.DOCUMENT_END) {
            Character currentCharacter = i < maskedCharacters.length ? maskedCharacters[i++] : null;

            if (currentCharacter == null) {
                logger.finest("current character: '<null>'");

                if (action == Action.DOCUMENT_BEGIN) {
                    logger.finest("current action: DOCUMENT_BEGIN");
                } else if (action == Action.STRING_PARSE) {
                    logger.finest("current action: STRING_PARSE");
                    if (logger.isLoggable(Level.FINER)) {
                        logger.finer("parsed string: " + parsedString.toString());
                    }
                } else if (action == Action.STRING_ESCAPE) {
                    logger.finest("current action: STRING_ESCAPE");
                    logger.fine("the string end with an escape char");
                    throw new MalformedInputException("the string end with an escape char");
                } else if (action == Action.FUNCTION) {
                    logger.finest("current action: FUNCTION");
                    logger.fine("the string end without define the function");
                    throw new MalformedInputException("the string end without define the function");
                } else if (action == Action.FUNCTION_NAME) {
                    logger.finest("current action: FUNCTION_NAME");
                    logger.fine("the string end without define the function name");
                    throw new MalformedInputException("the string end without define the function name");
                } else if (action == Action.FUNCTION_ARGS_BEGIN) {
                    logger.finest("current action: FUNCTION_ARGS_BEGIN");
                    logger.fine("the string end without define the function arguments");
                    throw new MalformedInputException("the string end without define the function arguments");
                } else if (action == Action.FUNCTION_ARGS_ESCAPE) {
                    logger.finest("current action: FUNCTION_ARGS_ESCAPE");
                    logger.fine("the string end with an escape char");
                    throw new MalformedInputException("the string end with an escape char");
                } else if (action == Action.FUNCTION_ARGS_END) {
                    logger.finest("current action: FUNCTION_ARGS_END");
                } else {
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.finest("current action: UNEXPECTED(" + action + ")");
                    }
                    final String errorMessage = String.format("unexpected action status [%s] at the end of string", action);
                    logger.fine(errorMessage);
                    throw new IllegalStateException(errorMessage);

                }

                action = Action.DOCUMENT_END;
            } else if (currentCharacter == '\\') {
                logger.finest("current character: \\");
                if (action == Action.DOCUMENT_BEGIN || action == Action.STRING_PARSE || action == Action.FUNCTION_ARGS_END) {
                    logger.finest("current action: DOCUMENT_BEGIN or STRING_PARSE or FUNCTION_ARGS_END");
                    action = Action.STRING_ESCAPE;
                } else if (action == Action.STRING_ESCAPE) {
                    logger.finest("current action: STRING_ESCAPE");
                    parsedString.append(currentCharacter);
                    action = Action.STRING_PARSE;
                } else if (action == Action.FUNCTION || action == Action.FUNCTION_NAME) {
                    logger.finest("current action: FUNCTION or FUNCTION_NAME");
                    logger.fine("escape char are not allowed as function name");
                    throw new MalformedInputException("escape char are not allowed as function name");
                } else if (action == Action.FUNCTION_ARGS_BEGIN || action == Action.FUNCTION_ARGS_VALUES) {
                    logger.finest("current action: FUNCTION_ARGS_BEGIN or FUNCTION_ARGS_VALUES");
                    action = Action.FUNCTION_ARGS_ESCAPE;
                } else if (action == Action.FUNCTION_ARGS_ESCAPE) {
                    logger.finest("current action: FUNCTION_ARGS_ESCAPE");
                    argumentValue.append(currentCharacter);
                    action = Action.FUNCTION_ARGS_VALUES;
                } else {
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.finest("current action: UNEXPECTED(" + action + ")");
                    }
                    final String errorMessage = String.format("unexpected action status [%s] for character [%s] at position [%d]", action, "\\", i);
                    logger.fine(errorMessage);
                    throw new IllegalStateException(errorMessage);
                }
            } else if (currentCharacter == '@') {
                logger.finest("current character: \\");
                if (action == Action.DOCUMENT_BEGIN || action == Action.STRING_PARSE || action == Action.FUNCTION_ARGS_END) {
                    logger.finest("current action: DOCUMENT_BEGIN or STRING_PARSE or FUNCTION_ARGS_END");
                    action = Action.FUNCTION;
                } else if (action == Action.STRING_ESCAPE) {
                    logger.finest("current action: STRING_ESCAPE");
                    parsedString.append(currentCharacter);
                    action = Action.STRING_PARSE;
                } else if (action == Action.FUNCTION || action == Action.FUNCTION_NAME) {
                    logger.finest("current action: FUNCTION or FUNCTION_NAME");
                    logger.fine("function char are not allowed as function name");
                    throw new MalformedInputException("function char are not allowed as function name");
                } else if (action == Action.FUNCTION_ARGS_BEGIN || action == Action.FUNCTION_ARGS_VALUES || action == Action.FUNCTION_ARGS_ESCAPE) {
                    logger.finest("current action: FUNCTION_ARGS_BEGIN or FUNCTION_ARGS_VALUES or FUNCTION_ARGS_ESCAPE");
                    // argumentValue.append(currentCharacter);
                    // action = Action.FUNCTION_ARGS_VALUES;
                    logger.fine("function char are not allowed as function argument");
                    throw new MalformedInputException("function char are not allowed as function argument");
                } else {
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.finest("current action: UNEXPECTED(" + action + ")");
                    }
                    final String errorMessage = String.format("unexpected action status [%s] for character [%s] at position [%d]", action, "@", i);
                    logger.fine(errorMessage);
                    throw new IllegalStateException(errorMessage);
                }
            } else if (currentCharacter == '(') {
                logger.finest("current character: (");
                if (action == Action.DOCUMENT_BEGIN || action == Action.STRING_PARSE || action == Action.STRING_ESCAPE || action == Action.FUNCTION_ARGS_END) {
                    logger.finest("current action: DOCUMENT_BEGIN or STRING_PARSE or STRING_ESCAPE or FUNCTION_ARGS_END");
                    parsedString.append(currentCharacter);
                    action = Action.STRING_PARSE;
                } else if (action == Action.FUNCTION) {
                    logger.finest("current action: FUNCTION");
                    logger.fine("empty function name is not allowed");
                    throw new MalformedInputException("empty function name is not allowed");
                } else if (action == Action.FUNCTION_NAME) {
                    logger.finest("current action: FUNCTION_NAME");
                    action = Action.FUNCTION_ARGS_BEGIN;
                } else if (action == Action.FUNCTION_ARGS_BEGIN || action == Action.FUNCTION_ARGS_VALUES || action == Action.FUNCTION_ARGS_ESCAPE) {
                    logger.finest("current action: FUNCTION_ARGS_BEGIN or FUNCTION_ARGS_VALUES or FUNCTION_ARGS_ESCAPE");
                    argumentValue.append(currentCharacter);
                    action = Action.FUNCTION_ARGS_VALUES;
                } else {
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.finest("current action: UNEXPECTED(" + action + ")");
                    }
                    final String errorMessage = String.format("unexpected action status [%s] for character [%s] at position [%d]", action, "(", i);
                    logger.fine(errorMessage);
                    throw new IllegalStateException(errorMessage);
                }
            } else if (currentCharacter == ')') {
                logger.finest("current character: (");
                if (action == Action.DOCUMENT_BEGIN || action == Action.STRING_PARSE || action == Action.STRING_ESCAPE || action == Action.FUNCTION_ARGS_END) {
                    logger.finest("current action: DOCUMENT_BEGIN or STRING_PARSE or STRING_ESCAPE or FUNCTION_ARGS_END");
                    parsedString.append(currentCharacter);
                    action = Action.STRING_PARSE;
                } else if (action == Action.FUNCTION) {
                    logger.finest("current action: FUNCTION");
                    logger.fine("empty function name is not allowed");
                    throw new MalformedInputException("empty function name is not allowed");
                } else if (action == Action.FUNCTION_NAME) {
                    logger.finest("current action: FUNCTION_NAME");
                    logger.fine("function is incomplete");
                    throw new MalformedInputException("function is incomplete");
                } else if (action == Action.FUNCTION_ARGS_BEGIN) {
                    logger.finest("current action: FUNCTION_ARGS_BEGIN");
                    parsedString.append(processFunction(functionName.toString(), null));
                    functionName.delete(0, functionName.length());
                    functionArguments.clear();
                    action = Action.FUNCTION_ARGS_END;
                } else if (action == Action.FUNCTION_ARGS_ESCAPE) {
                    logger.finest("current action: FUNCTION_ARGS_ESCAPE");
                    argumentValue.append(currentCharacter);
                    action = Action.FUNCTION_ARGS_VALUES;
                } else if (action == Action.FUNCTION_ARGS_VALUES) {
                    logger.finest("current action: FUNCTION_ARGS_VALUES");
                    functionArguments.add(argumentValue.toString());
                    argumentValue.delete(0, argumentValue.length());
                    parsedString.append(processFunction(functionName.toString(), functionArguments));
                    functionName.delete(0, functionName.length());
                    functionArguments.clear();
                    action = Action.FUNCTION_ARGS_END;
                } else {
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.finest("current action: UNEXPECTED(" + action + ")");
                    }
                    final String errorMessage = String.format("unexpected action status [%s] for character [%s] at position [%d]", action, ")", i);
                    logger.fine(errorMessage);
                    throw new IllegalStateException(errorMessage);
                }
            } else if (currentCharacter == ',') {
                logger.finest("current character: ,");
                if (action == Action.DOCUMENT_BEGIN || action == Action.STRING_PARSE || action == Action.STRING_ESCAPE || action == Action.FUNCTION_ARGS_END) {
                    logger.finest("current action: DOCUMENT_BEGIN or STRING_PARSE or STRING_ESCAPE or FUNCTION_ARGS_END");
                    parsedString.append(currentCharacter);
                    action = Action.STRING_PARSE;
                } else if (action == Action.FUNCTION || action == Action.FUNCTION_NAME) {
                    logger.finest("current action: FUNCTION or FUNCTION_NAME");
                    logger.fine("comma char in function name is not aLlowed");
                    throw new MalformedInputException("comma char in function name is not allowed");
                } else if (action == Action.FUNCTION_ARGS_BEGIN) {
                    logger.finest("current action: FUNCTION_ARGS_BEGIN");
                    functionArguments.add("");
                    action = Action.FUNCTION_ARGS_VALUES;
                } else if (action == Action.FUNCTION_ARGS_VALUES) {
                    logger.finest("current action: FUNCTION_ARGS_VALUES");
                    functionArguments.add(argumentValue.toString());
                    argumentValue.delete(0, argumentValue.length());
                    action = Action.FUNCTION_ARGS_VALUES;
                } else if (action == Action.FUNCTION_ARGS_ESCAPE) {
                    logger.finest("current action: FUNCTION_ARGS_ESCAPE");
                    argumentValue.append(currentCharacter);
                    action = Action.FUNCTION_ARGS_VALUES;
                } else {
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.finest("current action: UNEXPECTED(" + action + ")");
                    }
                    final String errorMessage = String.format("unexpected action status [%s] for character [%s] at position [%d]", action, ",", i);
                    logger.fine(errorMessage);
                    throw new IllegalStateException(errorMessage);
                }
            } else if (currentCharacter.toString().matches( "/[#$&|%§^]/" )) {
                // "/[' #\"$!.:;-\\[\\]{}?&%|^*+/§]/"
                if (logger.isLoggable(Level.FINEST)) {
                    logger.finest("current character: " + currentCharacter);
                }
                if (action == Action.DOCUMENT_BEGIN || action == Action.STRING_PARSE || action == Action.FUNCTION_ARGS_END) {
                    logger.finest("current action: DOCUMENT_BEGIN or STRING_ESCAPE or FUNCTION_ARGS_END");
                    String errorMessage = String.format("illegal character [%s] used in string without escape it at position [%d]", currentCharacter.toString(), i);
                    logger.fine(errorMessage);
                    throw new MalformedInputException(errorMessage);
                } else if (action == Action.STRING_ESCAPE) {
                    logger.finest("current action: STRING_PARSE");
                    parsedString.append(currentCharacter);
                    action = Action.STRING_PARSE;
                } else if (action == Action.FUNCTION || action == Action.FUNCTION_NAME) {
                    logger.finest("current action: FUNCTION or FUNCTION_NAME");
                    String errorMessage = String.format("illegal character [%s] used in function without escape it at position [%d]", currentCharacter.toString(), i);
                    logger.fine(errorMessage);
                    throw new MalformedInputException(errorMessage);
                } else if (action == Action.FUNCTION_ARGS_BEGIN || action == Action.FUNCTION_ARGS_VALUES || action == Action.FUNCTION_ARGS_ESCAPE) {
                    logger.finest("current action: FUNCTION_ARGS_BEGIN or FUNCTION_ARGS_VALUES or FUNCTION_ARGS_ESCAPE");
                    String errorMessage = String.format("illegal character [%s] used in function argument without escape it at position [%d]", currentCharacter.toString(), i);
                    logger.fine(errorMessage);
                    throw new MalformedInputException(errorMessage);
                } else {
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.finest("current action: UNEXPECTED(" + action + ")");
                    }
                    final String errorMessage = String.format("unexpected action status [%s] for character [%s] at position [%d]", action, currentCharacter.toString(), i);
                    logger.fine(errorMessage);
                    throw new IllegalStateException(errorMessage);
                }

                // TODO : [' #]
                // ': string delimiter
                //  : trimmed char in function
                // #: variable name group (like function)
            } else {
                if (logger.isLoggable(Level.FINEST)) {
                    logger.finest("current character: " + currentCharacter);
                }
                if (action == Action.DOCUMENT_BEGIN || action == Action.STRING_PARSE || action == Action.STRING_ESCAPE || action == Action.FUNCTION_ARGS_END) {
                    logger.finest("current action: DOCUMENT_BEGIN or STRING_PARSE or STRING_ESCAPE or FUNCTION_ARGS_END");
                    parsedString.append(currentCharacter);
                    action = Action.STRING_PARSE;
                } else if (action == Action.FUNCTION) {
                    logger.finest("current action: FUNCTION");
                    functionName.append(currentCharacter);
                    action = Action.FUNCTION_NAME;
                } else if (action == Action.FUNCTION_NAME) {
                    logger.finest("current action: FUNCTION_NAME");
                    functionName.append(currentCharacter);
                } else if (action == Action.FUNCTION_ARGS_BEGIN || action == Action.FUNCTION_ARGS_VALUES || action == Action.FUNCTION_ARGS_ESCAPE) {
                    logger.finest("current action: FUNCTION_ARGS_BEGIN or FUNCTION_ARGS_VALUES or FUNCTION_ARGS_ESCAPE");
                    argumentValue.append(currentCharacter);
                    action = Action.FUNCTION_ARGS_VALUES;
                } else {
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.finest("current action: UNEXPECTED(" + action + ")");
                    }
                    final String errorMessage = String.format("unexpected action status [%s] for character [%s] at position [%d]", action, currentCharacter.toString(), i);
                    logger.fine(errorMessage);
                    throw new IllegalStateException(errorMessage);
                }
            }
        }

        return parsedString.toString();
    }

    /**
     *
     * @param maskedString
     * @return
     */
    private String mask(final String maskedString) {
        return maskedString;
    }

    /**
     *
     * @param function
     * @param formatCallback
     *
     * @since 1.0
     */
    public void addFunction(final String function, final FormatCallback formatCallback) {
        Preconditions.checkNotNull(function, "function name cannot be null");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(function), "function name cannot be empty");
        Preconditions.checkNotNull(formatCallback, "formatCallback cannot be null");

        functions.put(function, formatCallback);
    }

    /**
     *
     * @param name
     * @param value
     *
     * @since 1.0
     */
    public void addReplacer(final String name, final String value) {
        Preconditions.checkNotNull(name, "replacer name cannot be null");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(name), "replacer name cannot be empty");
        Preconditions.checkNotNull(value, "value cannot be null");

        replacers.put(name, value);
    }

    /**
     *
     * @param name
     * @param arguments
     * @return
     *
     * @since 1.0
     */
    private String processFunction(final String name, final Collection<String> arguments) {

        FormatCallback formatCallback = functions.get(name);
        if (formatCallback == null) {
            if (logger.isLoggable(Level.FINE)) {
                logger.fine("not found callback for function " + name);
            }
            throw new NullPointerException("not found callback for function " + name);
        }

        if (logger.isLoggable(Level.FINER)) {
            logger.finer("process function " + name);
        }

        return formatCallback.parse(arguments != null ? (arguments.toArray(new String[arguments.size()])) : null);
    }

    /**
     *
     * @param format
     *
     * @since 1.0
     */
    public void setFormat(String format) {
        this.maskedString = format;
    }

    /**
     * Internal status action.
     */
    private static enum Action {
        DOCUMENT_BEGIN, STRING_PARSE, STRING_ESCAPE, FUNCTION, FUNCTION_NAME, FUNCTION_ARGS_BEGIN, FUNCTION_ARGS_END, FUNCTION_ARGS_VALUES, FUNCTION_ARGS_ESCAPE, DOCUMENT_END
    }
}
