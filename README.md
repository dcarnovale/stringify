# README #

Stringily is a Java library developed to simplify the string manipulation. It permit to search and replace token in strings faster than using the too often simple custom replacing.

## What is this repository for? ##

* Quick summary
* Version

## Installation ##

To install the library you simply need to compile and create a JAR library, than you can include in your project classpath and start using it.

### How do I get set up? ###

* The workspace is created with IntelliJ Idea CE. You can use any other editor you prefer. Instruction to convert from Idea to your IDE could be found with Internet Search engines.
* The project not required special configurations or dependency.
* Test case are written for JUnit 4.

## Usage ##

The use of the library is simple. Inside the source code you can find a StringifyTest.java JUnit source case with an example how to use it. The library need an initialization

```
#!java
Stringify stringify = new Stringify(SIMPLE_MASK_STRING);
stringify.addFunction(SubstrFormatCallback.SUBSTR_NAME, new SubstrFormatCallback()); // search for "@substr(MYSTRING, 2, 4)" 
stringify.addReplacer(TOKEN_NAME, REPLACER); // search for "#TOKEN_NAME"

```

after that you can simply call

```
#!java
String replaced = stringify.parse();

```

or, if you want change mask string

```
#!java
String replaced2 = stringify.parse(NEW_MASK_STRING);

```

## Contributing ##

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

### Contribution guidelines ###

* Every method must be tested with JUnit test. Previous JUnit test cannot be fail or modify without a specific authorization.
* The code must be review and approved before it can be merged in master tree.
* The code must be commented seriously. Comments like "this set variable", or "cycle all items" are unwelcome.
* No duplication, backdoor, or other wasting code is accepted.
* Any useful code or fix is always accepted.
* If you want became an admin of this project, please contact me with your references.

## History ##

The library has been created for a private project. The core of the library has been released as Open Source.

## Credits ##

Many thanks to d.carnovale as the author of the library.

### Who do I talk to? ###

* edruid

## License ##

This file is part of Stringify.

Stringify is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Stringify is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Stringify.  If not, see <http://www.gnu.org/licenses/>